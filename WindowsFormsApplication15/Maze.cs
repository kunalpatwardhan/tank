﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowsFormsApplication15
{

    
    public class Maze
    {
        public static int MAX_ROWS = 20;
        public static int MAX_COLS = 20;

        object[,] blocks = new Block[MAX_ROWS, MAX_COLS];

        public object[,] Blocks
        {
            get { return blocks; }
            set { blocks = value; }
        }


        public Maze()
        {
            int r, c;
            for(r=0;r<Maze.MAX_ROWS;r++)
            {
                for (c=0;c<Maze.MAX_COLS;c++)
                {
                    blocks[r, c] = null; //new Empty(r*c);
                }
            }
        }

    }
}
