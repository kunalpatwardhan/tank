﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WindowsFormsApplication15
{
    
    public partial class Form1 : Form
    {

        Maze m = new Maze();
        public Form1()
        {
            InitializeComponent();
        }


    
        void refreshGrid()
        {
            int r, c;
            for(c=0;c<Maze.MAX_COLS;c++)
            {
                for(r=0;r<Maze.MAX_ROWS;r++)
                {

                    dataGridView1.Rows[r].Cells[c].Value = m.Blocks[r,c];
                    if (m.Blocks[r, c] == null)
                        dataGridView1.Rows[r].Cells[c].Style.BackColor = Color.White;
                    else
                        dataGridView1.Rows[r].Cells[c].Style.BackColor = ((Block)m.Blocks[r, c]).BackColor;
                    
                }
            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            refreshGrid();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.RowCount = Maze.MAX_ROWS;
            dataGridView1.ColumnCount = Maze.MAX_COLS;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }
        int r,c;

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            r = dataGridView1.SelectedCells[0].RowIndex;
            c = dataGridView1.SelectedCells[0].ColumnIndex;
           // m.Blocks[r, c] = null;
            

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewTextBoxCell c1 in dataGridView1.SelectedCells)
            {
                r = c1.RowIndex;
                c = c1.ColumnIndex;
                m.Blocks[r, c] = new Brick(r * c);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            m.Blocks[r, c] = new AI(r * c,m,r,c);
        }
      

    }
}
