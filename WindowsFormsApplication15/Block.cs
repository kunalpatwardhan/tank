﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
namespace WindowsFormsApplication15
{
    public enum BlockType
    { Empty, Brick, Metal, AI, Player }

    public enum Direction
    { Left,Right,Down,Up}


    public abstract class Block
    {
        public static Random rn = new Random();
        BlockType blockType;
        Color backColor;

        public Color BackColor
        {
            get { return backColor; }
            set { backColor = value; }
        }

        public BlockType BlockType
        {
            get { return blockType; }
            set { blockType = value; }
        }


        int identity;

        public int Identity
        {
            get { return identity; }
            set { identity = value; }
        }


        int row;

        public int Row
        {
            get { return row; }
            set { row = value; }
        }


        int col;

        public int Col
        {
            get { return col; }
            set { col = value; }
        }

        Maze maze;

        public Maze M
        {
            get { return maze; }
            set { maze = value; }
        }
        public abstract override string ToString();
    }

    public class Empty : Block
    {

        public Empty(int identity)
        {
            Identity = identity;
            
        }

        public override string ToString()
        {
            //return identity.ToString();
            return "";
        }

    }


    public class Brick : Block
    {
        public Brick(int identity)
        {
            this.Identity = identity;
            BackColor = Color.Brown;
        }

        public override string ToString()
        {
            return "B" + Identity.ToString();
        }

    }

    public class AI : Block
    {
        Timer t;
        Direction d;
        public AI(int identity, Maze m,int r,int c)
        {
            this.M = m;
            this.Identity = identity;
            this.Row = r;
            this.Col = c;

            BackColor = Color.Red;

            ChangeDirection();

            t = new Timer(timer_tick,this,500,500);
            
        }

        public override string ToString()
        {
            return "A" + Identity.ToString();
        }

        bool MoveRight()
        {
            if (Col + 1 < Maze.MAX_COLS)
            {
                if (M.Blocks[Row, Col + 1] == null)
                {
                    M.Blocks[Row, Col] = null;
                    Col++;
                    M.Blocks[Row, Col] = this;
                    return true;
                }
            }
            return false;
        }

        bool MoveLeft()
        {
            if (Col - 1 >= 0)
            {
                if (M.Blocks[Row, Col - 1] == null)
                {
                    M.Blocks[Row, Col] = null;
                    Col--;
                    M.Blocks[Row, Col] = this;
                    return true;
                }
            }
            return false;
        }

        bool MoveDown()
        {
            if (Row + 1 < Maze.MAX_ROWS)
            {
                if (M.Blocks[Row+1, Col] == null)
                {
                    M.Blocks[Row, Col] = null;
                    Row++;
                    M.Blocks[Row, Col] = this;
                    return true;
                }
            }
            return false;
        }

        bool MoveUp()
        {
            if (Row - 1 >= 0)
            {
                if (M.Blocks[Row - 1, Col] == null)
                {
                    M.Blocks[Row, Col] = null;
                    Row--;
                    M.Blocks[Row, Col] = this;
                    return true;
                }
            }
            return false;
        }

        void ChangeDirection()
        {
            d = (Direction)rn.Next(0, 4);
        }
        void timer_tick(object o)
        {
            bool success=false;
            switch (d)
            {
                case Direction.Right:
                    success= MoveRight();
                    break;
                case Direction.Left:
                    success = MoveLeft();
                    break;
                case Direction.Up:
                    success = MoveUp();
                    break;
                case Direction.Down:
                    success = MoveDown();
                    break;
            }

            if(success==false)
            {
                ChangeDirection();
            }
        }
    }
}
